-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: vs1-prod-20200819-production-vpc-new.cda4cc5d7cuk.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_audio_data_live`
--

DROP TABLE IF EXISTS `tbl_audio_data_live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_audio_data_live` (
  `audio_data_live_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int unsigned DEFAULT NULL,
  `site_id` int unsigned DEFAULT NULL,
  `audio_device_id` bigint unsigned DEFAULT NULL,
  `audio_call_id` bigint unsigned DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `total_mos` double DEFAULT NULL,
  `total_jitter` int DEFAULT NULL,
  `total_latency` int DEFAULT NULL,
  `total_packetloss` double DEFAULT NULL,
  `health` int DEFAULT NULL,
  `qos` int DEFAULT NULL,
  `video` json DEFAULT NULL,
  `audio` json DEFAULT NULL,
  `health_info` json DEFAULT NULL,
  `call_type` varchar(127) DEFAULT NULL,
  `call_handle` varchar(45) DEFAULT NULL,
  `call_state` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`audio_data_live_id`)
) ENGINE=InnoDB AUTO_INCREMENT=204772 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_audio_data_live`
--

LOCK TABLES `tbl_audio_data_live` WRITE;
/*!40000 ALTER TABLE `tbl_audio_data_live` DISABLE KEYS */;
INSERT INTO `tbl_audio_data_live` VALUES (204727,433,492,3683,31079,'2023-09-19 21:19:37',4.5,0,0,0.00350253,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 0}','{\"codec\": \"4:G.722.1C\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 44447}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','421cb008','Connected'),(204728,433,480,3552,31045,'2023-09-19 21:19:38',4.5,0,0,0.00208039,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 0}','{\"codec\": \"4:G.722.1C\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 3, \"packets_expected\": 123284}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','42556008','Connected'),(204729,433,480,3582,31071,'2023-09-19 21:19:38',4.5,2,0,0.002898,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 0}','{\"codec\": \"4:G.722.1C\", \"jitter\": 2, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 42717}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','42255008','Connected'),(204730,433,480,3541,31074,'2023-09-19 21:19:38',0,0,0,0,100,1,'{\"codec\": \"\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 0}','{\"codec\": \"\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 0}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Incoming','4238e008','Connected'),(204732,433,480,3606,31073,'2023-09-19 21:19:38',4.5,1,0,0.0318167,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 19, \"packets_expected\": 25065}','{\"codec\": \"4:G.722.1C\", \"jitter\": 1, \"latency\": 0, \"packets_lost\": 2, \"packets_expected\": 38468}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','42769008','Connected'),(204733,433,480,3599,31080,'2023-09-19 21:19:38',4.5,0,0,0.197848,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 16, \"packets_expected\": 1763}','{\"codec\": \"4:G.722.1C\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 3803}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','42a66008','Connected'),(204734,433,492,3662,31076,'2023-09-19 21:19:38',4.5,1,0,0.0038978,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 2, \"packets_expected\": 31774}','{\"codec\": \"4:G.722.1C\", \"jitter\": 1, \"latency\": 0, \"packets_lost\": 2, \"packets_expected\": 44324}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','47515008','Connected'),(204735,433,480,3668,31070,'2023-09-19 21:19:38',4.5,1,0,0.0205518,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 0}','{\"codec\": \"4:G.722.1C\", \"jitter\": 1, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 48239}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','42ab4008','Connected'),(204736,433,480,3560,31065,'2023-09-19 21:19:38',4.5,0,0,0.00574647,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 0}','{\"codec\": \"4:G.722.1C\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 2, \"packets_expected\": 52822}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','4212a008','Connected'),(204738,433,480,3530,31072,'2023-09-19 21:19:39',4.5,1,0,0,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 0}','{\"codec\": \"4:G.722.1C\", \"jitter\": 1, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 43567}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','477c0008','Connected'),(204739,433,480,3567,31062,'2023-09-19 21:19:39',4.5,1,0,0,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 7576}','{\"codec\": \"4:G.722.1C\", \"jitter\": 1, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 47690}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','41e88008','Connected'),(204740,433,480,3594,31078,'2023-09-19 21:19:39',4.5,0,0,0,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 16118}','{\"codec\": \"4:G.722.1C\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 21465}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','422da008','Connected'),(204741,433,492,329,30831,'2023-09-19 21:19:39',4.5,1,0,0.00169449,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 20, \"packets_expected\": 771987}','{\"codec\": \"4:G.722.1C\", \"jitter\": 1, \"latency\": 0, \"packets_lost\": 9, \"packets_expected\": 1290260}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','47979008','Connected'),(204742,433,492,3740,31077,'2023-09-19 21:19:39',0,0,0,0,100,1,'{\"codec\": \"\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 0}','{\"codec\": \"\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 0}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Incoming','d3f2d1c0','Connected'),(204744,433,492,3682,31075,'2023-09-19 21:19:39',4.5,1,0,0,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 24075}','{\"codec\": \"4:G.722.1C\", \"jitter\": 1, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 44931}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','491fa008','Connected'),(204745,433,480,3538,31049,'2023-09-19 21:19:40',4.5,0,0,0.00261386,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 87385}','{\"codec\": \"4:G.722.1C\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 3, \"packets_expected\": 134832}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','426ec008','Connected'),(204747,433,480,3621,31051,'2023-09-19 21:19:40',4.5,3,0,0.0022659,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 0}','{\"codec\": \"4:G.722.1C\", \"jitter\": 3, \"latency\": 0, \"packets_lost\": 8, \"packets_expected\": 139140}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','42a60008','Connected'),(204748,433,480,3671,31057,'2023-09-19 21:19:41',4.5,0,0,0,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 0}','{\"codec\": \"4:G.722.1C\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 98197}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','47393008','Connected'),(204749,433,480,3564,31060,'2023-09-19 21:19:42',4.5,0,0,0.0211787,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 20, \"packets_expected\": 33901}','{\"codec\": \"4:G.722.1C\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 1, \"packets_expected\": 52924}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','41e6a008','Connected'),(204766,433,574,3479,31066,'2023-09-19 21:23:41',4,1,0,0.660154,88,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 223, \"packets_expected\": 31430}','{\"codec\": \"4:G.722.1C\", \"jitter\": 1, \"latency\": 0, \"packets_lost\": 287, \"packets_expected\": 61299}','{\"qos\": 1, \"health\": 87.5, \"healthPoint\": 4.0, \"healthTrigger\": 0.5, \"notificationTriggers\": 0}','Outgoing','42266008','Connected'),(204767,433,575,3457,31082,'2023-09-19 21:23:49',4.5,2,0,0,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 0}','{\"codec\": \"4:G.722.1C\", \"jitter\": 2, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 17378}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','423e5008','Connected'),(204768,433,575,3456,31081,'2023-09-19 21:23:49',4.5,0,0,0.0157196,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 8, \"packets_expected\": 14774}','{\"codec\": \"4:G.722.1C\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 22594}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','42976008','Connected'),(204769,433,575,3724,31063,'2023-09-19 21:23:49',4.5,2,0,0,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 26583}','{\"codec\": \"4:G.722.1C\", \"jitter\": 2, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 64931}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','516de008','Connected'),(204770,433,575,3463,31044,'2023-09-19 21:23:49',4.5,1,0,0.000360453,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 0}','{\"codec\": \"4:G.722.1C\", \"jitter\": 1, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 223714}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','41ed6008','Connected'),(204771,433,575,3460,31067,'2023-09-19 21:23:50',4.5,1,0,0.00891215,100,1,'{\"codec\": \"38:\", \"jitter\": 0, \"latency\": 0, \"packets_lost\": 0, \"packets_expected\": 26106}','{\"codec\": \"4:G.722.1C\", \"jitter\": 1, \"latency\": 0, \"packets_lost\": 5, \"packets_expected\": 60060}','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}','Outgoing','4720c008','Connected');
/*!40000 ALTER TABLE `tbl_audio_data_live` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-19 20:04:17
