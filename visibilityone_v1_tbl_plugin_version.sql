-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: vs1-prod-20200819-production-vpc-new.cda4cc5d7cuk.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_plugin_version`
--

DROP TABLE IF EXISTS `tbl_plugin_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_plugin_version` (
  `plugin_version_id` int NOT NULL AUTO_INCREMENT,
  `version` varchar(127) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `stamp` datetime DEFAULT CURRENT_TIMESTAMP,
  `active` int DEFAULT '0',
  `file_size` varchar(50) DEFAULT NULL,
  `release_notes` longtext,
  `email_notes` longtext,
  `update_schedule` datetime DEFAULT NULL,
  `update_status` enum('OPEN','SCHEDULED','UPDATING','COMPLETED','FAILED') DEFAULT NULL,
  `include_release_notes` tinyint DEFAULT '0',
  PRIMARY KEY (`plugin_version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_plugin_version`
--

LOCK TABLES `tbl_plugin_version` WRITE;
/*!40000 ALTER TABLE `tbl_plugin_version` DISABLE KEYS */;
INSERT INTO `tbl_plugin_version` VALUES (1,'0.10.4.59','VisibilityOnePluginSetup-0.10.4.59.exe','2020-02-27 07:22:55',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(2,'0.11.0.61','VisibilityOnePluginSetup-0.11.0.61.exe','2020-02-27 17:39:03',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(3,'0.11.2.64','VisibilityOnePluginSetup-0.11.2.64.exe','2020-08-12 21:34:27',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(4,'1.0.31','VisibilityOnePluginSetup-1.0.31.exe','2020-10-09 09:52:40',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(5,'1.0.34.0','VisibilityOnePluginSetup-1.0.34.0.exe','2020-11-03 22:39:19',0,'19MB','notes',NULL,NULL,'COMPLETED',0),(6,'1.0.35.0','VisibilityOnePluginSetup-1.0.35.0.exe','2020-11-24 04:05:45',0,'19MB','Added Zip Data (Zip VisibilityOne Plugin Files) button',NULL,NULL,'COMPLETED',0),(7,'1.0.36.0','VisibilityOnePluginSetup-1.0.36.0.exe','2021-03-06 01:43:37',0,'19MB','Added fix for unable to send data',NULL,NULL,'COMPLETED',0),(8,'1.0.37.0','VisibilityOnePluginSetup-1.0.37.0.exe','2021-03-24 23:30:21',0,'19MB','Added fix for cpu info',NULL,NULL,'COMPLETED',0),(9,'1.0.38.0','VisibilityOnePluginSetup-1.0.38.0.exe','2021-03-30 09:14:59',0,'19MB','Fixed cpu info error when collector is not installed',NULL,'2021-04-09 17:00:00','COMPLETED',0),(10,'1.0.39.0','VisibilityOnePluginSetup-1.0.39.0.exe','2021-04-24 07:05:16',0,'19MB','Added silent installation',NULL,NULL,'COMPLETED',0),(11,'1.0.40.0','VisibilityOnePluginSetup-1.0.40.0.exe','2021-04-29 21:58:25',0,'19MB','Fixed ping error caused by unavailable audio render',NULL,NULL,'COMPLETED',0),(12,'1.0.41.0','VisibilityOnePluginSetup-1.0.41.0.exe','2021-05-03 22:38:08',0,'19MB','CPU usage optimization',NULL,'2021-05-05 00:00:00','COMPLETED',0),(13,'1.0.42.0','VisibilityOnePluginSetup-1.0.42.0.exe','2021-06-16 07:51:23',0,'19MB','Added unlink plugin',NULL,NULL,'COMPLETED',0),(14,'1.0.43.0','VisibilityOnePluginSetup-1.0.43.0.exe','2021-07-05 22:06:41',0,'19MB','Added wifi, mac, ip host info',NULL,NULL,'COMPLETED',0),(15,'1.0.45.0','VisibilityOnePluginSetup-1.0.45.0.exe','2022-02-24 02:57:48',0,'22MB','Added teams support',NULL,NULL,'COMPLETED',0),(16,'1.0.47.0','VisibilityOnePluginSetup-1.0.47.0.exe','2022-03-09 21:34:10',0,'22MB','Removed pathsolution calls in app simulator',NULL,NULL,'COMPLETED',0),(17,'1.0.48.0','VisibilityOnePluginSetup-1.0.48.0.exe','2022-03-10 07:53:02',0,'22MB','Added call simulator actions',NULL,NULL,'COMPLETED',0),(18,'1.0.49.0','VisibilityOnePluginSetup-1.0.49.0.exe','2022-03-21 08:22:41',0,'22MB','Added crestron client list',NULL,NULL,'COMPLETED',0),(19,'1.0.50.0','VisibilityOnePluginSetup-1.0.50.0.exe','2022-03-31 10:58:55',0,'22MB','Fixed login error in some PCs due to mac address',NULL,NULL,'COMPLETED',0),(20,'1.0.51.0','VisibilityOnePluginSetup-1.0.51.0.exe','2022-04-07 12:14:16',0,'22MB','Fixed disconnection issue due to cpu info error in WMI',NULL,NULL,'COMPLETED',0),(31,'1.0.52.0','VisibilityOnePluginSetup-1.0.52.0.exe','2022-04-25 17:26:02',0,'22MB','Changed determining of teams room installation',NULL,NULL,'COMPLETED',0),(32,'1.0.55.0','VisibilityOnePluginSetup-1.0.55.0.exe','2022-10-18 00:00:00',0,'22.9MB','Added support for extended IoT monitornig',NULL,NULL,'COMPLETED',0),(33,'1.0.56.2','VisibilityOnePluginSetup-1.0.56.2.exe','2022-12-21 00:00:00',0,'23.9MB','Bug Fixes',NULL,NULL,'COMPLETED',0),(34,'1.0.58.0','VisibilityOnePluginSetup-1.0.58.0.exe','2023-02-13 00:00:00',0,'23.8MB','Minor Bug Fixes',NULL,NULL,'COMPLETED',0),(35,'1.0.60.0','VisibilityOnePluginSetup-1.0.60.0.exe','2023-02-20 18:56:00',0,'29.5MB','Bug Fixes',NULL,NULL,'COMPLETED',0),(36,'1.3.0','VisibilityOnePluginSetup-1.3.0exe','2023-02-21 18:56:00',0,'29.5MB','Bug Fixes',NULL,NULL,'COMPLETED',0),(37,'1.1.0','VisibilityOnePluginSetup-1.1.0.exe','2023-02-22 18:56:00',0,'29.5MB','Bug Fixes',NULL,NULL,'COMPLETED',0),(38,'1.0.7','VisibilityOnePluginSetup-1.0.7.exe','2023-02-23 18:56:00',0,'29.5MB','Bug Fixes',NULL,NULL,'COMPLETED',0),(39,'2.0.0.4','VisibilityOnePluginSetup-2.0.0.4.exe','2023-04-30 00:00:00',0,'23.5MB','Minor bug fixes and new features',NULL,'2023-05-08 19:45:00','COMPLETED',0),(40,'1.0.53.0','VisibilityOnePluginSetup-1.0.53.0.exe','2022-04-26 17:26:02',0,'22MB','Changed determining of teams room installation',NULL,NULL,'COMPLETED',0),(41,'1.0.54.0','VisibilityOnePluginSetup-1.0.54.0.exe','2022-04-26 17:26:02',0,'22MB','Changed determining of teams room installation',NULL,NULL,'COMPLETED',0),(42,'2.0.0.6','VisibilityOnePluginSetup-2.0.0.6.exe','2023-05-19 17:26:02',0,'22MB','Changed determining of teams room installation',NULL,NULL,'COMPLETED',0),(43,'2.1.5.0','VisibilityOnePluginSetup-2.1.5.0.exe','2023-09-07 00:00:00',0,'91.7MB','Logitech Sync Support',NULL,NULL,'COMPLETED',0),(44,'2.1.7.0','VisibilityOnePluginSetup-2.1.7.0.exe','2023-09-19 00:00:00',1,'91.7MB','Bug Fix for Logitech Sync',NULL,NULL,'OPEN',0);
/*!40000 ALTER TABLE `tbl_plugin_version` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-19 20:06:26
